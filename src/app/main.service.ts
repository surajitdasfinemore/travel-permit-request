import { UtilityService } from './services/utility.service';
import { LoadingService } from './services/loading.service';
import { ConstantService } from 'src/app/services/constant.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError, BehaviorSubject } from 'rxjs';
import { map, catchError } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class MainService {
  currentTicketInfo = {};

  constructor(private constantService: ConstantService,
    private loadingService: LoadingService,
    private utilityService: UtilityService,
    private http: HttpClient) { }

  // Method for getting application type list
  getUserDetails() {
    this.loadingService.showLoading();
    return this.http
      .get(this.constantService.restUrls.userDetails,
            this.utilityService.getHTTPHeaderForAjaxCalls())
      .pipe(
        map((res: any) => {
          this.loadingService.hideLoading();
          return <any>res.uiResponse;
        }),
        catchError((err) => {
          this.loadingService.hideLoading();
          return throwError(err);
        })
      )
  }

  // Method for preparing request for getting travel request list
  prepareRequestForTravelRequestList(userId) {
    return {
      header: {
        userId: this.constantService.userName,
        sessionId: this.constantService.sessionId,
        application: this.constantService.appName
      },
      uiRequest: {
        userId: userId
      }
    }
  }

  // Method for getting all travel request list
  getTravelRequestList(userId) {
    this.loadingService.showLoading();
    return this.http
      .post(this.constantService.restUrls.getApplicationType,
            this.prepareRequestForTravelRequestList(userId),
            this.utilityService.getHTTPHeaderForAjaxCalls())
      .pipe(
        map((res: any) => {
          this.loadingService.hideLoading();
          return <any>res.uiResponse;
        }),
        catchError((err) => {
          this.loadingService.hideLoading();
          return throwError(err);
        })
      )
  }

  // Method for preparing request for search travel request
  prepareRequestForSearchTravelRequest(searchkey) {
    return {
      header: {
        userId: this.constantService.userName,
        sessionId: this.constantService.sessionId,
        application: this.constantService.appName
      },
      uiRequest: {
        key: searchkey
      }
    }
  }

  // Method for getting travel reqeust details by searchkey
  getSearchTravelRequest(searchkey) {
    this.loadingService.showLoading();
    return this.http
      .post(this.constantService.restUrls.getSearchReqeust,
            this.prepareRequestForSearchTravelRequest(searchkey),
            this.utilityService.getHTTPHeaderForAjaxCalls())
      .pipe(
        map((res: any) => {
          this.loadingService.hideLoading();
          return <any>res.uiResponse;
        }),
        catchError((err) => {
          this.loadingService.hideLoading();
          return throwError(err);
        })
      )
  }

  // Method for getting application type list
  getApplicationType() {
    this.loadingService.showLoading();
    return this.http
      .get(this.constantService.restUrls.getApplicationType,
            this.utilityService.getHTTPHeaderForAjaxCalls())
      .pipe(
        map((res: any) => {
          this.loadingService.hideLoading();
          return <any>res.uiResponse;
        }),
        catchError((err) => {
          this.loadingService.hideLoading();
          return throwError(err);
        })
      )
  }

  // Method for getting application subtype list
  getApplicationSubType() {
    this.loadingService.showLoading();
    return this.http
      .get(this.constantService.restUrls.getApplicationSubtype,
            this.utilityService.getHTTPHeaderForAjaxCalls())
      .pipe(
        map((res: any) => {
          this.loadingService.hideLoading();
          return <any>res.uiResponse;
        }),
        catchError((err) => {
          this.loadingService.hideLoading();
          return throwError(err);
        })
      )
  }

  // Method for getting application subtype list
  getApplicationReasonType() {
    this.loadingService.showLoading();
    return this.http
      .get(this.constantService.restUrls.getApplicationReason,
            this.utilityService.getHTTPHeaderForAjaxCalls())
      .pipe(
        map((res: any) => {
          this.loadingService.hideLoading();
          return <any>res.uiResponse;
        }),
        catchError((err) => {
          this.loadingService.hideLoading();
          return throwError(err);
        })
      )
  }

  // Method for getting province list
  getProvinceList() {
    this.loadingService.showLoading();
    return this.http
      .get(this.constantService.restUrls.getProvinceList,
            this.utilityService.getHTTPHeaderForAjaxCalls())
      .pipe(
        map((res: any) => {
          this.loadingService.hideLoading();
          return <any>res.uiResponse;
        }),
        catchError((err) => {
          this.loadingService.hideLoading();
          return throwError(err);
        })
      )
  }

  // Method for preparing request for submit travel request
  prepareRequestForSubmitRequest(userSelected) {
    return {
      header: {
        userId: this.constantService.userName,
        sessionId: this.constantService.sessionId,
        application: this.constantService.appName
      },
      uiRequest: userSelected
    }
  }

  // Method for sending request for submit travel request
  submitTravelReqeust(userSelected) {
    this.loadingService.showLoading();
    return this.http
      .post(this.constantService.restUrls.submitTravelRequest,
            this.prepareRequestForSubmitRequest(userSelected),
            this.utilityService.getHTTPHeaderForAjaxCalls())
      .pipe(
        map((res: any) => {
          this.loadingService.hideLoading();
          return <any>res.uiResponse;
        }),
        catchError((err) => {
          this.loadingService.hideLoading();
          return throwError(err);
        })
      )
  }

  // Method for preparing request for approve/reject on travel request
  prepareRequestForAction(id, action) {
    return {
      header: {
        userId: this.constantService.userName,
        sessionId: this.constantService.sessionId,
        application: this.constantService.appName
      },
      uiRequest: {
        requestId: id,
        action: action
      }
    }
  }

  // Method for approve/reject travel request Rest call
  approveRejectRequest(id, action) {
    this.loadingService.showLoading();
    return this.http
      .post(this.constantService.restUrls.approveRejectRequest,
            this.prepareRequestForAction(id, action),
            this.utilityService.getHTTPHeaderForAjaxCalls())
      .pipe(
        map((res: any) => {
          this.loadingService.hideLoading();
          return <any>res.uiResponse;
        }),
        catchError((err) => {
          this.loadingService.hideLoading();
          return throwError(err);
        })
      )
  }
}
