import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {
  userType = '';
  constructor() { }

  // Method for getting user type from login user
  getUserType() {
    this.userType;
  }

  // Method for seting user type from login user
  setUserType(type) {
    this.userType = type;
  }
}
