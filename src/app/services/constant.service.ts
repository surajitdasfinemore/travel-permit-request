import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstantService {
  normalUser = 'normal';
  adminRoleName = 'CBDDO';

  appName = 'Travel Permit Request';
  SAMLToken =  '';
  userName = 'surajit';
  sessionId = '';
  appUrl = 'http://sag-tec-dev-03:5555';
  userRoles = 'CBDDO';
  adminRoles = '';
  fullName = 'Surajit Das';

  // appName = window['appName'];
  // SAMLToken =  window['SAMLToken'];
  // userName = window['userName'];
  // sessionId = window['sessionId'];
  // appUrl = window['endpointAddress'];
  // userRoles = window['userRoles'];
  // adminRoles = window['adminRoles'];
  // fullName = window['fullName'];

  restUrls = {
    SAMLUrl: this.appUrl,
    userDetails: this.appUrl + '/api/userDetails',
    getSearchReqeust: this.appUrl + '/api/getSearchReqeust',
    getApplicationType: this.appUrl + '/api/getApplicationType',
    getApplicationSubtype: this.appUrl + '/api/getApplicationSubtype',
    getApplicationReason: this.appUrl + '/api/getApplicationReason',
    getProvinceList: this.appUrl + '/api/getProvinceList',
    submitTravelRequest: this.appUrl + '/api/submitTravelRequest',
    approveRejectRequest: this.appUrl + '/api/approveRejectRequest',
  };

  displayNames = {

  };

  // All rest call messages are to be configured here
  errMessages = {
    somethingWentWrong: 'Something went wrong. Please contract Administrator.',
    notImplemented: 'This feature is not available as of now. Please check later.',
    ticketCreated: 'Expense has been created successfully',
    approveTicket: 'Do you want to APPROVE this request ?',
    returnForCorrectionTicket: 'Do you want to RETURN this request for CORRECTION ?',
    rejectTicket: 'Do you want to REJECT this request ?',
    supportedFiles: 'Only supported file extensions are JPEG, PNG and PDF.',
    submitTravelRequest: 'Travel request has been sent successfully.'
  };

  validationMsgs = {

  };

  constructor() { }
}
