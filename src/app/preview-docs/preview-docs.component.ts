import { MainService } from './../main.service';
import { UtilityService } from './../services/utility.service';
import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-preview-docs',
  templateUrl: './preview-docs.component.html',
  styleUrls: ['./preview-docs.component.css']
})
export class PreviewDocsComponent implements OnInit {
  @Input('documentList') documentList;
  @Output() close: EventEmitter<boolean> = new EventEmitter();
  showFile = false;
  imgUrl = '';
  pdfUrl:any = '';
  constructor(private mainService: MainService,
    private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
  }

  closePopup() {
    this.close.emit(false);
  }

  showDocumentPreview(event, doc) {
    document.querySelectorAll('.preview-docs > .preview-container .doc.active').forEach(ele => ele.classList.remove('active'));
    this.showFile = true;
    this.pdfUrl = '';
    this.imgUrl = '';
    if (doc.extension === 'image/jpeg' || doc.extension === 'image/jpg' || doc.extension === 'image/png') {
      this.imgUrl = doc.base64;
    } else if (doc.extension === 'application/pdf') {
      this.pdfUrl = this.sanitizer.bypassSecurityTrustResourceUrl(doc.base64);
    }
    if (event.target.classList.contains('extension')) {
      event.target.parentNode.classList.add('active');
    } else {
      event.target.classList.add('active');
    }
  }

  closeFile() {
    this.showFile = false;
    this.imgUrl = '';
    this.pdfUrl = '';
  }
}
