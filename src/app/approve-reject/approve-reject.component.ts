import { Router } from '@angular/router';
import { MainService } from './../main.service';
import { UtilityService } from './../services/utility.service';
import { MessageBoxComponent } from './../components/message-box/message-box.component';
import { MatDialog } from '@angular/material';
import { ConstantService } from 'src/app/services/constant.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConfirmationBoxComponent } from '../components/confirmation-box/confirmation-box.component';

@Component({
  selector: 'app-approve-reject',
  templateUrl: './approve-reject.component.html',
  styleUrls: ['./approve-reject.component.css']
})
export class ApproveRejectComponent implements OnInit, OnDestroy {
  subscriptions = [];
  userSelected = {
      "requestId": "ABCD123456",
      "applicationType": "type 1",
      "applicationSubtype": "subtype 1",
      "reason": "reason1",
      "reasonDesc": "My brother is sick",
      "refDocs": [
          {
              "fileId": "",
              "ticketId": "",
              "filename": "findmorelogo.jpg",
              "extension": "image/jpeg",
              "size": 6587,
              "base64": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCADIAMgDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9U6KKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACikJwKTcKAHUwsckYFZut+J9M8OWjXOp3kVnCP4pXAz7CvH/FP7UWi6e8kekWcuoS9BI52oP615eMzTB4BXxNRR8uv3bnq4LKsbmLthaTl59Pv2PcfM5waXzPbFfH2t/tN+ML/AHCzlttOQ9oolc/mwNcVqXxh8a37MX8S6jHn/nhcNFj8FIFfIVuNsvp6U4Sl8kvzZ9xh+AMzqq9WcY/Nv8kfewkz0oL4Ga/PSX4o+MxjHi3XAPbUZv8A4qnW3xk8cWDBo/FWqyEc/v7p5B+RJri/19wifvUZfej0f+Ib41r3a8b+jP0IEuSeKPM+lfEGjftV+OtKZRc3UGpxj+G4gUH8wAa9P8Kftk6TdskGvaZJp0h6zwHfH+XJr2sJxhlWKkoubg/7y/VaHg43gXOsInKNNVF/dd39zs/uR9JjpS1z3hTx7oXjSyW50bUre+TAyI3BZfYjtW8JAexr7GnVhVip05Jp9VqfCVaVSjN06sXGS3TVmPopquHzinVqZBRRRQAUUUUAFFFFABRRRQAUUUUAFISFGT0oJA6nFZviPX7Pw1o9zqN7IEggXcfUnIAA+pIFRKcYRc5OyWrLhCVSShBXb2LN9qNtp9pLcXEyQwxruZ2OABXhHxC/aL8nzbPw1tL52m9kGQP90f1rgPiP8T9V8eXTR/vLbS1bKWqkgN7t6muDkhcjiNs9ORxivybOeKqtVuhgNI/zdX6dvzP1/JOEqVJKvmHvS/l6L17/AJBrutah4gvGu9Ru5bqduryuWNY0wx/CDV+WN/7rflVWSCVztWNmb0CmvzSo6lSTlK7b76n6zRjCnFRirJFGU5zVOSth9FvyOLOc/wDADVWTQ9R/58p/+/ZrndKp/K/uO+nVp/zL7zHl7VVk6GtibQtRGM2Nx/37NZ93YXNr/roJIs/3lIrmqU5rdM9CnUg3ZSRny/0qAk7cGp5ODjv6VCwwM9vWuXY9BbD9N1e+0G9S8066msbpDkSwSFG/MV9F/Cz9ry6tWh0/xiPtMIO3+0Y1wyjtuA6/Wvmp/mzjmq7g8joO2Ote9lub4vLKnPh52XVdH6o8jNMiwGdUvZ4umm+j+0vR7/ofqPoWv6fr+mxX+n3Ud1aTAMksZBBFaSuHJA7V+cHwp+Met/CnUw9lcPPprtmexc5Rx3IHY/SvvL4b/EfRviToUOp6VcI+VAlh3fPE3dWHav3fJOIaGcR5fhqLeP6rv+h/M3EvCeK4enz/AB0XtLt5NdPyZ19FNDgnAIzTq+tPhQooooAKKKKACiiigAooqOQ8jJ49KAI725js7dpppFhiQZaRzgKPeufk8feF5F2Premup6hp0wf1r5F/bU+PDXV1J4E0a5YQI2dTkjb7xByI/wA+T9K+QHOSSOhHftX6HlXCEsww0a9epyc2ytfTu9Ufmua8ZRwGKlh8PT5+Xd3tr5aM/XVfGnhFGDDVdJUjkETJxVj/AIWJ4Y/6D2n/APgQv+NfkAOtXtG0i88Qapbadp8Et1e3LiOGGNcs7HsK9h8CYemruu0v8K/zPIXHuIqNJYdN/wCJv9D9cF+IHht3CLrlgzMcACdSSfzrdjYSqGU5UjIIr5Y+AP7HGmeFkttc8ZQx6lrAxJHYN80Nue24dGP14FfVcCLHCioAqqMAKMACvzjMaGDw9b2eEqOaW7tZfLufpOWYjGYql7XGUlTb2V7v56aeggGO2aOR0FS0V5R7FiL5j7UyaBZ0KSKsiHgq4yDViiiwbao4TxJ8GPBvilHF7oFmHP8Ay1t4hFJn13LgmvDPH37Hflo9z4Wvy5GT9jvDnP8Aut/jX1dRXgY7IcvzBP21JX7pWf3r9T6fLeJc1yuS9hWbivst3X3P9LH5e+JfDOp+FNVk07VrN7O7TrHIuMj1B7isc9BX6VeP/hxofxG0mWw1e1jlxnypwMSRE91PUfSvg/4u/CXU/hX4hezuEaaylJNreY+WRfQ+46fhX4xn3DNfKP3sHzUu/Vev+Z/Q3C3GGGz39xVXJXtt0fmv8t/U4B+MnGe1dd8LPidqXws8TJqVhITA+Fubc8LKg9R61yIOSeelNYcH0r5XD16mGqxq0pWktmj9CxOFpYyhKhiIqUZKzTP0+8D+MNP8deH7HWdNlElrdRBx6qe6n3BzXR18EfswfF6TwD4sTSb6Y/2NqThSO0cp4DfjX3fEd20ryODmv6PyLNo5vhFV+2tJLz/yZ/H3E+QVOH8c6G9OWsX3X+a2f/BLFFFFfRHyIUUUUAFFFFABXnnx4+JcXwp+G+q63uX7aIjFZo38U7cJ+ROT7A137sVJ9K+Cf28fiJJrXjWw8LwS5tdMi82ZR0aVhxn6DIr6DIsvWZY+nQl8K1fov89vmfOcQZj/AGZl9StH4novV/5b/I+X9Rv59TvJbq5keWeZi8juxJLE85NV+W96VugpF+99Oa/o6MVFKK2P5pbcncns7WW/uoLe3iaa4mcRxRRjLOxOAAPWv0V/Zd/Z3tfhTocer6tBHL4ovYwXdwCbZD/Ap7E968X/AGG/gvFrutSeONUgL2mnsY7BX6NMeGfHfaD+ZB7V91+Uvv8AnX5Bxdnc5zeXUHovifd9vRdT9j4PyKMYLMa61fwrsu/r2FCKMcClAxS0V+YH6sFFFFABRRRQAUUUUANKKeqiuU+I/gPT/iJ4Zu9HvolZJF/duVyY3xwwPautppQE5/rWNWlCvTlSqK8Xo0bUa1TD1I1qTtKLun2Z+XnjTwre+B/E1/oeoApc2khTJH3l6q34gg1hMAeo/Ovsv9sT4WJq+hx+LbKEm8slEdzt6vFkkE/TJ59MV8ankmv5szvLJZVjZ0Ps7r0f+Wx/ZXDOdRz3L4Yr7e0l2kt/v3XqMJ2kMDgjoa/QH9mD4jHx98OLOO4lMmpaZi0nLtlmCj5GPqSu3J9c1+frjdx2PBr2v9krxwfC3xPh06SQra6svkMp6F8/LXq8LZg8Dj4xb92fuv8AR/eeRxzk6zTKKk4r36XvL5br5r8Uj76oqNWPGTUlf0IfyMFFFFABRRRQBXunECNK7bEUFmY9FA6mvyL+JfimXxn491zWpSS15dPKMnOAScAe1fqR8ZdTbRfhT4yvkOJIdHu2jOf4vJbb+uK/JOVt7ljwWOTX6xwLh1+/xD30X6v9D8g49xDvQw621f6f5jSc1PptjNqeo2tnbqXuLiVYo1HdmIAH5kVABk16r+y14aTxT8efCVnKMxR3D3ZOOMxRvKv/AI8gr9NxddYbD1K7+ym/uVz8uwlB4rEU6C+1JL73Y/Rv4VeDLb4f+BdG0K1QItrAoYgcsxHJPvXZVGsZVs8Y9qkr+XqlSVWcqk927n9V0qcaNONOCskrBRRRWZqFFFFABRRRQAUUUUAFFFFAGbr2lwa1pd3Y3KiSC4iaN0PQg1+ZHjLw8/hPxVq2jyAg2VzJCM9wGIU/iMH8a/USUcgj0r4O/a80NdJ+L91cqoQahbRXGB7KIyf/AByvzTjnCqeEp4lbxdvk/wDgo/Z/DHHOlmFXBN6Tjf5xf+Tf3HiDDJq5oWpS6NrVjfQNtltplkRvQg5zVNutMPWvxeEnCSlHdH9JzgqkXCWz0P1W0jUI9X0mxvoWzDdQpMh9VZQw/nWlXnHwF1L+1vhB4SnY5KWEUOT/ALChP6V6PX9U4Wr7fD06y+0k/vR/CONw/wBVxVXD/wAkmvudgooorqOMKKKKAPLP2nZDH8DPF+CQTZMK/LButfqv+0hYtqHwP8aRqCSmmTTcDsiFj/KvyoYc/Wv2fgdr6nVX979EfiXHaf1yk/7v6sF69cCvo39hC1Nx8bBLkYh0+dyD74Xj/vqvnIHGcV75+xHqUVh8e9KgYgG7t7mJSfaF3/8AZK+rz1SlluIUf5X+Wp8hkMoxzPDuX86/PT8T9JqKQMD0Ipa/m0/pwKKKKACiiigAooooAKKKKACiiigBj18aftvxqPGOgvj5zYYJ9vMevsuRtuM8V8V/ttXyy+P9HtVwfK04McdiZH4/LFfE8YNLKZ37x/M/SfDxN8QU7fyy/I+cW60xjg09utMbqc9K/AEf1qfoN+yncST/AAP8Psf4XuE/ATOB/KvYq8l/ZhtfsXwR8OIQVLJJIM/7UjN/WvWq/p/KE1l2HT/kj+SP4g4galm+LcdvaT/9KYUUUV6x4AUUUUAYfjXRv+Ej8J63pOM/b7Ge1x/vxsv9a/H68hNvdSxt1Ryv5Gv2Zc4b8K/Lb9p3wU3gb4ya9aBCttcym7gOONjnIA+lfp/A2KUa1bDS6pNfLf8ANH5Tx5hHKlRxS+y2n89V+TPKa6/4Q+Kv+EK+J3hrWi/lxWt7GZW9I2O1/wDx1mrkKVeDX63WpxrU5UpbSTT+Z+QUasqNSNWG8WmvkfszazrcwRSxnKOAwx6EVZrw39kr4nx/ET4YWcMs4k1TSgLW4UnkgD5W+hwfyr3Kv5gxeFngsRPD1FrF2P6owWKhjcPDEU9pK4UUUVyHaFFFFABRRRQAUUUUAFFFFAEUpwcnpivzw/aR8TDxP8YfEEqNuhtZFtE9vLRVb/x4NX3D8W/G1v4A8Eajq0zAOkZSFD1dyOAK/NW7u5b67nuJnMk0rmR3P8TMck/jnNflHHWNSp08FF6t8z+Wi/U/dvDDLZOtWzKa0S5F6uzf3WS+ZXbrSAFnAAyScUrDP5V1Hwt8Jt40+IGh6OIy8dxcR+bjsgI3GvyihRlXqRpQ3k0vvP37E14YWjOvUdoxTb9Fqfob8LNGPh/4b+F9OIAe3023ST/f8td365rsKgWMKoAHA6VPX9T0KapUo010SX3H8JV60sRWnWlvJt/e7hRRRW5gFFFFACFQeozXyj+3l8Lm1vwnZeL7KDdcaSfLuio/5YMcbj/unafpmvq+s7xBotp4j0a90u/iE1leQvBNGf4kYEH9DXp5bjp5bi4YmH2Xr5rqvuPKzTAQzLCVMNP7S08n0/E/G9wByOh7Ui8H1rufjJ8Mr34UeOr/AES7RvKDl7aYjiWLPysK4YcH3/Qiv6WoVoV6catN3jLVH8w16E8PUlSqK0ouzPUf2efjDcfBz4gWups7tpNyPIvoF/ijP8WPVTg/hX6d6Nrdr4h0q01OwukubK6RZIpY2yrKe4Ir8cyxOMnivov9l79pmT4WXQ0HXnkm8NTsNsmdxtHJ+8P9n1r4HinIJY2H1vDL95Far+Zf5r8T9A4V4gWAn9UxT/dyej/lf+T/AAP0Wpay9H12z1/Tre/0+7iu7K4UPFPAwdGHsRWkhJUE9a/FWmnZ7n7jGSklKOqY6iiikUFFFFABRRRQAVXlYgv8xUDnPpStKRnJAx3r5j/aS/aIXT47rwt4duUku3BS8uoWz5QP8II74rysyzKhleHeIrvbZdW+yPbyfKMTneKjhcKtXu+iXd/1qzzf9qf4uf8ACb+J/wCwtOuDLpOmNsZlbKyzD7x/Dp+BrwjoSMc0/liSc89z1NMfoSMAk9a/m7H46rmOJniK28vw7I/sfKcsoZRhKeEoL3Yr731b9Rj9DX1J+xR4ANxqOoeLbmLMUQNrasw/i/jx9On4V82eHdAvPFGu2WlWMbTXV1KI41UZzk9fpX6WfDzwdb+AfCOlaHbcpaQqjMP436s34tk/jX2fB2WPE4v61Ne7T/F9Pu3Pz3xFzuOCy9YCk/fraekVu/nt9504AHaloor9zP5fCiiigAooooAKaybj1xTqKAPHP2jPgFa/GrwwkULx2uu2rbrS7cYA9VYjsa+YD+wD42PH9r6Wf+BP/hX6AOOKyvEmrv4e0i41BbKfUBCoYw24zIRkZx9Bz+FfQYPiTHZTQdOlJci11V7eh83jOGcBm2IVSrB87stHa/Q+FB+wB43B/wCQvpX/AH03+FKf2AfGxx/xN9KGOmGb/CvpqP8Aaa0JnAbSr+NeQWJTj8M1I37TPhtf+XK9/If41yrxPk9sRD/wE7f+IVpO31af/gR5p8E/gT8WPg5qcSW+vabd6HLIPtVhKzFSM8sgxw3uOtfVcOfKXIwcc4rxlv2n/DS9bO8P4Clj/al8MZVXtL1R/e2Aj9K+ax3F2AzCr7avWgpeStf1PqMBwZmOXUvZUKE3Hs3e3p5HtFFeNt+1N4QUcreD/tkajP7Vvg4D7t5/36NcH9vZWv8AmIj956a4czd7YaX3HtFNdtqkgZ9q8Xb9rLwWi5IvM/3fJP8AhWRqv7Y3hq3RltNKv7t8cHCqv45NZT4iymmuZ4iPy1/I1hwvnVR8scLL5q35nv3me2DWR4k8YaV4Q0577WL6Cwtlz88zhd3soPU+wr5L8V/td+JtUV4tJtLfSozkCTHmPj8eleJeIfEureKb77Xq2oT39x2edy20egz0FfKY/jnCUU44OLnLu9F/n/W59rlfhzjsRJTx81Tj2Wsv8l63foe3fGX9qa68SLcaT4YEljpz5El4wKySD29Afzr5ylJkfeWYkkk5PWppARnn8MVC2fQ1+TZhmmJzOt7bESu+i6L0R+9ZRlGDyagqGEhZdX1b7t/15EZ6mmEkEYGfb1pxbk/p719L/s1fs5y63LbeKfElt5diuJLSzmXmXnh2B/h9PXrV5ZluIzPERoUFr1fRLuzTOM4wmSYN4rEvTourfZf1odn+yf8ABFvC+mx+KdZg8vVL1N1rDIvzQxHoT6Fhz+Ir6REQDbicn6URxeWe2MYqSv6Sy7AUctw8cPR2X4vuz+O83zTEZ1jJ4zEvWWy6JdEvQKKKK9I8cKKKKACiiigAooooAQ0ki71K9j1p1FAHivxS+A8etmXU9CjSC85aS2Bwsn07A187atptzpF69peQSW9whwY5FINfeTDKke1ct4v+HmkeNrQxanaq8gHyTKAJFP1r4HOOFqWMbrYX3Zvp0f8AkfoWS8WVsClQxac4d+q/zX4nxBKSc81TlAOf517X40/Zw13R99xo2NVthyYxxIB9O/4V49q2l3mkTvb31rNaTqcGOdCjD8DX5LjcuxWAly4iDX5ffsftGXZng8xjzYaopeXX5rcypARnnIqpIDV2QHGccVTkrxGfSU7FWbPfH4VTkQZz1q5L2qs47d8VzzO+CRWkqBzg1O/Jp2n6Veazdi10+1nvblukVvGXb8hzWMYynLlirs6+ZQjzSdkjPkOc/wAqWx0651e8is7OCS6uZW2pDEpZm+gFe4+Af2TPFHih0uNbU6DYnGRMMzMP9zt+OK+pfhz8HfD3wzs/L0qzzdMMSXko3SP+PYe1ff5Twljcc1OsvZw7vf5L/Ox+fZ1x5l2VxdPDv2tTsvhXrL9Fc8Y+Bf7Kp0t4Nb8XwpJdDDw6ecFUPUF/U+1fT8EIhXaqhVAwAOgpYl2g9eueakr9ty3LMNldH2OGjbu+r9T+c83znGZ3iPrGMnd9F0S7Jf1cKKKK9U8QKKKKACiiigAooooAKKKKACiiigAooooAQjIxWXq+gadrlu1vf2UN3Ef4ZUDA1q0mKicIzXLNXRUZSg+aDszynWf2cPBusbnSwaxkPe3cqo/4D0rjtQ/ZC0udibbXbqHn7rRKQK+iKTFeFW4fyuu7zoK/lp+Vj6PD8SZxhlaniZW89fzufMb/ALHKtMT/AMJCRF7Rc1btf2M9LDbrrxBdyLnlEiUAj619I0tcceFcni7+wT9W/wDM7pcY55JW+sNfKP8AkeNaL+yx4H0phJPaTajIP+flyVP/AAHpXpWh+FdK8M2q2+l6dbWEI/hgjCj9K26K9vDZdg8H/ApRj6Jf8OeBi80x2O0xVaU12bdvu2EHQUtFFeieWFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB/9k="
          }
      ],
      "relatives": [],
      "userPermanentDetails": {
          "address": "Discovery gardens, Dubai, UAE",
          "phone": "+971-12345678",
          "email": "surajit.das@findmore.pt"
      },
      "userCommunicationDetails": {
          "address": "Discovery gardens, Dubai, UAE",
          "phone": "+971-12345678",
          "email": "surajit.das@findmore.pt"
      },
      "selectedProvinceList": [
          {
              "id": "1",
              "name": "Province 1"
          },
          {
              "id": "2",
              "name": "Province 2"
          }
      ]
  }
  showDocsPreview = false;
  constructor(private constantService: ConstantService,
    private dialog: MatDialog,
    private utilityService: UtilityService,
    private mainService: MainService,
    private router: Router) { }

  ngOnInit() {
  }

  openDocumentPreview() {
    this.showDocsPreview = true;
  }

  onClose(status) {
    this.showDocsPreview = status;
  }

  approveTravelRequest() {
    const dialogRef = this.dialog.open(ConfirmationBoxComponent, {
      width: '600px',
      panelClass: 'confirm-box-panel',
      data: {
        message: this.constantService.errMessages.approveTicket
      },
      disableClose: true
    });

    const subs = dialogRef.afterClosed().subscribe((response) => {
      if (this.utilityService.isDefined(response) && response === true) {
        const sub = this.mainService.approveRejectRequest(this.userSelected.requestId, 'approved').subscribe(res => {
          if (res.status.statusCode === '1') {
            const ref = this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });

            ref.afterClosed().subscribe(() => {
              this.router.navigate(['/', 'travel-request-dashboard']);
            });

          } else {
            const ref = this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
            ref.afterClosed().subscribe(() => {
              this.router.navigate(['/', 'travel-request-dashboard']);
            });

          }
        }, err => {
          const ref = this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.somethingWentWrong
            },
            disableClose: true
          });
          ref.afterClosed().subscribe(() => {
            this.router.navigate(['/', 'travel-request-dashboard']);
          })

        });
        this.subscriptions.push(sub);
      }
    });
    this.subscriptions.push(subs);
  }

  rejectTravelRequest() {
    const dialogRef = this.dialog.open(ConfirmationBoxComponent, {
      width: '600px',
      panelClass: 'confirm-box-panel',
      data: {
        message: this.constantService.errMessages.rejectTicket
      },
      disableClose: true
    });

    const subs = dialogRef.afterClosed().subscribe((response) => {
      if (this.utilityService.isDefined(response) && response === true) {
        const sub = this.mainService.approveRejectRequest(this.userSelected.requestId, 'rejected').subscribe(res => {
          if (res.status.statusCode === '1') {
            const ref = this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });

            ref.afterClosed().subscribe(() => {
              this.router.navigate(['/', 'travel-request-dashboard']);
            });

          } else {
            const ref = this.dialog.open(MessageBoxComponent, {
              width: '50vw',
              panelClass: 'message-box-panel',
              data: {
                message: res.status.message
              },
              disableClose: true
            });
            ref.afterClosed().subscribe(() => {
              this.router.navigate(['/', 'travel-request-dashboard']);
            });

          }
        }, err => {
          const ref = this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.somethingWentWrong
            },
            disableClose: true
          });
          ref.afterClosed().subscribe(() => {
            this.router.navigate(['/', 'travel-request-dashboard']);
          })

        });
        this.subscriptions.push(sub);
      }
    });
    this.subscriptions.push(subs);
  }

  goback() {
    this.utilityService.back();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
