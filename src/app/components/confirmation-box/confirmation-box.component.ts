import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, OnDestroy, Inject, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-confirmation-box',
  templateUrl: './confirmation-box.component.html',
  styleUrls: ['./confirmation-box.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ConfirmationBoxComponent implements OnInit, OnDestroy {
  subscriptions = [];
  message = '';
  constructor(public dialogRef: MatDialogRef<ConfirmationBoxComponent>,
    @Inject(MAT_DIALOG_DATA) public service: any) { }

  ngOnInit() {
    this.message = this.service.message;
  }

  // Method for closing dialog based on the user action
  closeDialog(status) {
    this.dialogRef.close(status);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
