import { Component, OnInit, OnDestroy, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-information-box',
  templateUrl: './information-box.component.html',
  styleUrls: ['./information-box.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class InformationBoxComponent implements OnInit, OnDestroy {

  subscriptions = [];
  constructor(public dialogRef: MatDialogRef<InformationBoxComponent>,
    @Inject(MAT_DIALOG_DATA) public information: any) { }

  ngOnInit() {
  }

  // Method for closing a dialog box with ref pass to its parent
  closeDialog(data) {
    this.dialogRef.close(data);
  }

  // Method for releasing the memory on component destroy
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
