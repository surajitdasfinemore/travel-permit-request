import { ConstantService } from 'src/app/services/constant.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  subscriptions = [];
  fullname;
  constructor(private router: Router,
    private constantService: ConstantService) { }

  ngOnInit() {
    this.fullname = this.constantService.fullName;
  }

  goHome() {
    this.router.navigate(['/']);
  }

  // Method for releasing memory on destroy
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
