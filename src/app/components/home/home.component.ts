import { ConstantService } from './../../services/constant.service';
import { HeaderService } from './../../services/header.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  reqButtonName = 'Travel Permit Request';
  constructor(private router: Router,
    private headerService: HeaderService,
    private constantService: ConstantService) { }

  ngOnInit() {
    if (this.headerService.userType === this.constantService.adminRoleName) {
      this.reqButtonName = 'Review Travel Request';
    }
  }

  gotoTravelRequestDashboard() {
    this.router.navigate(['/', 'travel-request-dashboard']);
  }
}
