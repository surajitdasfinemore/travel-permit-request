import { Component, OnInit, OnDestroy, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-comment-box',
  templateUrl: './comment-box.component.html',
  styleUrls: ['./comment-box.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CommentBoxComponent implements OnInit, OnDestroy {
  subscriptions = [];
  message = '';
  comment= '';
  type;
  constructor(public dialogRef: MatDialogRef<CommentBoxComponent>,
    @Inject(MAT_DIALOG_DATA) public service: any) { }

  ngOnInit() {
    this.message = this.service.message;
  }

  // Method for closing dialog based on the user action
  closeDialog(status) {
    this.dialogRef.close(status);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
