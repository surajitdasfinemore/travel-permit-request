import { HeaderService } from './services/header.service';
import { ConstantService } from './services/constant.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  subscriptions = [];
  userType = '';
  constructor(private constantService: ConstantService,
    private headerService: HeaderService) {
  }

  ngOnInit() {
    // TODO: SAML Request for creating session with Integration Server
    // this.http.post(this.constantService.restUrls.SAMLUrl,
    //     'SAMLResponse=' + window['SAMLToken'], {
    //     headers: new HttpHeaders({
    //       'Content-Type' : 'x-content/samlassertion',
    //       'sag-authentication-not-required': 'sag-authentication-not-required'
    //     })
    // }).subscribe(res => {
    //   console.log("SAML Successfully done", res);
    // }, err => {
    //   console.error("SAML Failed", err);
    // });

    // Setting up userType based on the logged in user role
    this.updateUserType();
  }

  // Method for updating the user type based on the logged user roles
  updateUserType() {
    if (this.constantService.userRoles.indexOf(this.constantService.adminRoleName) !== -1) {
      this.userType = this.constantService.adminRoleName;
    } else {
      this.userType = this.constantService.normalUser;
    }
    this.headerService.setUserType(this.userType);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
