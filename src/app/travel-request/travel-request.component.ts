import { Router } from '@angular/router';
import { MainService } from './../main.service';
import { ConstantService } from './../services/constant.service';
import { MessageBoxComponent } from './../components/message-box/message-box.component';
import { UtilityService } from './../services/utility.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-travel-request',
  templateUrl: './travel-request.component.html',
  styleUrls: ['./travel-request.component.css']
})
export class TravelRequestComponent implements OnInit, OnDestroy {

  subscriptions:any = [];
  userSelected = {
    applicationType: '',
    applicationSubtype: '',
    reason: '',
    reasonDesc: '',
    refDocs: [],
    relatives: [],
    userPermanentDetails: {
      address: 'Discovery gardens, Dubai, UAE',
      phone: '+971-12345678',
      email: 'surajit.das@findmore.pt'
    },
    userCommunicationDetails: {
      address: 'Discovery gardens, Dubai, UAE',
      phone: '+971-12345678',
      email: 'surajit.das@findmore.pt'
    },
    selectedProvinceList: []
  }
  relations = [
    {
      id: 'father',
      value: 'Father'
    },
    {
      id: 'mother',
      value: 'Mother'
    },
    {
      id: 'son',
      value: 'Son'
    },
    {
      id: 'daughter',
      value: 'Daughter'
    },
    {
      id: 'grandson',
      value: 'Grand Son'
    },
    {
      id: 'granddaughter',
      value: 'Grand Daughter'
    },
    {
      id: 'others',
      value: 'Others'
    }
  ];
  relativeRef = {name:'', age: '', relative: '', others: ''}
  maxNoOfRelatives = [{id:1,value:1},{id:2,value:2},{id:3,value:3},{id:4,value:4},{id:5,value:5},{id:6,value:6}];
  noOfRelatives:any = 0;

  applicationType = [
    {
      type: 'type 1',
      name: 'Application Type 1'
    },
    {
      type: 'type 2',
      name: 'Application Type 2'
    },
    {
      type: 'type 3',
      name: 'Application Type 3'
    },
    {
      type: 'type 4',
      name: 'Application Type 4'
    },
    {
      type: 'type 5',
      name: 'Application Type 5'
    },
  ];
  applicationSubType = [
    {
      type: 'subtype 1',
      name: 'Application Subtype 1'
    },
    {
      type: 'subtype 2',
      name: 'Application Subtype 2'
    },
    {
      type: 'subtype 3',
      name: 'Application Subtype 3'
    },
    {
      type: 'subtype 4',
      name: 'Application Subtype 4'
    },
    {
      type: 'subtype 5',
      name: 'Application Subtype 5'
    }
  ];
  reasonForTravelList = [
    {
      type: 'reason1',
      name: 'Reason type 1'
    },
    {
      type: 'reason2',
      name: 'Reason type 2'
    },
    {
      type: 'reason3',
      name: 'Reason type 3'
    },
    {
      type: 'reason4',
      name: 'Reason type 4'
    },
    {
      type: 'reason5',
      name: 'Reason type 5'
    },
    {
      type: 'reason6',
      name: 'Reason type 6'
    },
  ];
  availableProvinceList = [
    {
      id: '1',
      name: 'Province 1'
    },
    {
      id: '2',
      name: 'Province 2'
    },
    {
      id: '3',
      name: 'Province 3'
    },
    {
      id: '4',
      name: 'Province 4'
    },
    {
      id: '5',
      name: 'Province 5'
    },
    {
      id: '6',
      name: 'Province 6'
    },
    {
      id: '7',
      name: 'Province 7'
    },
    {
      id: '8',
      name: 'Province 8'
    },
    {
      id: '9',
      name: 'Province 9'
    },
    {
      id: '10',
      name: 'Province 10'
    }
  ];
  availableSelectList = [];
  availableProvinceListCopy = [];
  selectAvilList = [];
  selectSelectedList = [];

  slideX = 0;
  acceptance = false;
  samePhone = true;
  sameEmail = true;
  sameAddress = true;

  showDocsPreview = false;
  constructor(private utilityService: UtilityService,
    private dialog: MatDialog,
    private constantService: ConstantService,
    private mainService: MainService,
    private router: Router) { }

  ngOnInit() {
    const sub = this.mainService.getApplicationType().subscribe(res => {
      this.applicationType = res;
    }, err => {
      console.error(err);
    });
    this.subscriptions.push(sub);

    const sub1 = this.mainService.getApplicationSubType().subscribe(res => {
      this.applicationSubType = res;
    }, err => {
      console.error(err);
    });
    this.subscriptions.push(sub1);

    const sub2 = this.mainService.getApplicationReasonType().subscribe(res => {
      this.reasonForTravelList = res;
    }, err => {
      console.error(err);
    });
    this.subscriptions.push(sub2);

    const sub3 = this.mainService.getProvinceList().subscribe(res => {
      this.availableProvinceList = res;
      this.availableProvinceListCopy = JSON.parse(JSON.stringify(this.availableProvinceList));
    }, err => {
      console.error(err);
    });
    this.subscriptions.push(sub3);
  }

  goBack() {
    this.utilityService.back();
  }

  // Method for moving the page based on the user clicks on the tabs and it provides the slide bar effect
  navigateTo(tabno) {
    this.slideX = 100 * (tabno - 1) * -1;
  }

  // Method for converting file stream to base64
  toBase64(file, index) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.userSelected.refDocs[index].base64 = reader.result;
    }
  }

  // Method for reading file drops in file drop zone
  fileDrop(files) {
    for (let index = 0; index < files.length; index++) {
      if (!(files[index].type === 'application/pdf'
          || files[index].type === 'image/jpg'
          || files[index].type === 'image/jpeg'
          || files[index].type === 'image/png')) {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.supportedFiles
            },
            disableClose: true
          });
          return;
      }
    }
    for (let index = 0; index < files.length; index++) {
      this.userSelected.refDocs.push({
        fileId: '',
        ticketId: '',
        filename: files[index].name,
        extension: files[index].type,
        size: files[index].size,
        base64: this.toBase64(files[index], this.userSelected.refDocs.length)
      });
    }
  }

  // Method for removing uploading files
  removeFile(index) {
    this.userSelected.refDocs.splice(index, 1);
  }

  // Method for clicking upload file internally
  clickUploadFile() {
    document.getElementById('fileUploadInput').click();
  }

  // Method for uploading files using input file html
  uploadFiles(event) {
    for (let index = 0; index < event.target.files.length; index++) {
      if (!(event.target.files[index].type === 'application/pdf'
          || event.target.files[index].type === 'image/jpg'
          || event.target.files[index].type === 'image/jpeg'
          || event.target.files[index].type === 'image/png')) {
          this.dialog.open(MessageBoxComponent, {
            width: '50vw',
            panelClass: 'message-box-panel',
            data: {
              message: this.constantService.errMessages.supportedFiles
            },
            disableClose: true
          });
          return;
      }
    }

    this.userSelected.refDocs = [];
    for(let index = 0; index < event.target.files.length; index++) {
      this.userSelected.refDocs.push({
        fileId: '',
        ticketId: '',
        filename: event.target.files[index].name,
        extension: event.target.files[index].type,
        size: event.target.files[index].size,
        base64: this.toBase64(event.target.files[index], index)
      });
    }
  }

  updateNoOfRelatives() {
    if (this.userSelected.relatives.length === this.noOfRelatives) {
      return;
    } else if (this.noOfRelatives > this.userSelected.relatives.length) {
      for (let index = this.userSelected.relatives.length; index < this.noOfRelatives; index++) {
        this.userSelected.relatives.push(JSON.parse(JSON.stringify(this.relativeRef)));
      }
    } else {
      for (let index = this.userSelected.relatives.length; index >= this.noOfRelatives; index--) {
        this.userSelected.relatives.splice(index, 1);
      }
    }
  }

  updateRelation(rel) {
    if (rel.relation !== 'others') {
      rel.others = '';
    }
  }

  disableOnRelatives() {
    if (this.noOfRelatives === 0) {
      return false;
    }

    for (let index = 0; index < this.userSelected.relatives.length; index++) {
      if (this.userSelected.relatives[index].name === ''
          || this.userSelected.relatives[index].age === ''
          || this.userSelected.relatives[index].relative === ''
          || (this.userSelected.relatives[index].relative === 'others' && this.userSelected.relatives[index].others === '')) {
        return true;
      }
    }
    return false;
  }

  disabledPreview() {
    if (!this.samePhone && this.userSelected.userCommunicationDetails.phone === '') {
      return true;
    }

    if (!this.sameEmail && this.userSelected.userCommunicationDetails.email === '') {
      return true;
    }

    if (!this.sameAddress && this.userSelected.userCommunicationDetails.address === '') {
      return true;
    }

    if (this.availableSelectList.length === 0) {
      return true;
    }
    return false;
  }

  updatePhone() {
    this.userSelected.userCommunicationDetails.phone = '';
  }

  updateEmail() {
    this.userSelected.userCommunicationDetails.email = '';
  }

  updateAddress() {
    this.userSelected.userCommunicationDetails.address = '';
  }

  updatePhoneAsPermanent() {
    this.samePhone = true;
    this.userSelected.userCommunicationDetails.phone = this.userSelected.userPermanentDetails.phone;
  }

  updateEmailAsPermanent() {
    this.sameEmail = true;
    this.userSelected.userCommunicationDetails.email = this.userSelected.userPermanentDetails.email;
  }

  updateAddressAsPermanent() {
    this.sameAddress = true;
    this.userSelected.userCommunicationDetails.address = this.userSelected.userPermanentDetails.address;
  }

  clickOnProviceList(event, p, listName) {
    if (listName === 'avail-list') {
      if (event.target.classList.contains('active')) {
        event.target.classList.remove('active');
        this.selectAvilList = this.selectAvilList.filter(list => list.id !== p.id);
      } else {
        event.target.classList.add('active');
        this.selectAvilList.push(JSON.parse(JSON.stringify(p)));
      }
    } else if (listName === 'selected-list') {
      if (event.target.classList.contains('active')) {
        event.target.classList.remove('active');
        this.selectSelectedList = this.selectSelectedList.filter(list => list.id !== p.id);
      } else {
        event.target.classList.add('active');
        this.selectSelectedList.push(JSON.parse(JSON.stringify(p)));
      }
    }
  }

  addProvince() {
    for (let index = 0; index < this.selectAvilList.length; index++) {
      this.availableSelectList.push(JSON.parse(JSON.stringify(this.selectAvilList[index])));
    }

    for (let index = this.availableProvinceList.length - 1; index >= 0; index--) {
      for (let subIdx = 0; subIdx < this.selectAvilList.length; subIdx++) {
        if (this.availableProvinceList[index].id === this.selectAvilList[subIdx].id) {
          this.availableProvinceList.splice(index, 1);
        }
      }
    }

    this.selectAvilList = [];
  }

  removeProvince() {
    for (let index = 0; index < this.selectSelectedList.length; index++) {
      this.availableProvinceList.push(JSON.parse(JSON.stringify(this.selectSelectedList[index])));
    }

    for (let index = this.availableSelectList.length - 1; index >= 0; index--) {
      for (let subIdx = 0; subIdx < this.selectSelectedList.length; subIdx++) {
        if (this.availableSelectList[index].id === this.selectSelectedList[subIdx].id) {
          this.availableSelectList.splice(index, 1);
        }
      }
    }

    this.selectSelectedList = [];
  }

  removeAllProvince() {
    this.availableProvinceList = JSON.parse(JSON.stringify(this.availableProvinceListCopy));
    this.availableSelectList = [];
    this.selectAvilList = [];
    this.selectSelectedList = [];
  }

  preview() {
    this.userSelected.selectedProvinceList = JSON.parse(JSON.stringify(this.availableSelectList));
    this.navigateTo(6);
  }

  openDocumentPreview() {
    this.showDocsPreview = true;
  }

  onClose(status) {
    this.showDocsPreview = status;
  }

  sendRequest() {
    console.log(this.userSelected);
    const sub = this.mainService.submitTravelReqeust(this.userSelected).subscribe(res => {
      const dialogRef = this.dialog.open(MessageBoxComponent, {
        width: '50vw',
        panelClass: 'message-box-panel',
        data: {
          message: this.constantService.errMessages.submitTravelRequest
        },
        disableClose: true
      });

      dialogRef.afterClosed().subscribe(() => {
        this.goBack();
      });
    }, err => {
      console.log(err);
    });
    this.subscriptions.push(sub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
