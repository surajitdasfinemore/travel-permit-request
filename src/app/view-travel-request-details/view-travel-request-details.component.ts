import { Router } from '@angular/router';
import { UtilityService } from './../services/utility.service';
import { MainService } from './../main.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as html2pdf from 'html2pdf.js';

@Component({
  selector: 'app-view-travel-request-details',
  templateUrl: './view-travel-request-details.component.html',
  styleUrls: ['./view-travel-request-details.component.css']
})
export class ViewTravelRequestDetailsComponent implements OnInit, OnDestroy {

  currentTicket = {};
  constructor(private mainService: MainService,
    private utilityService: UtilityService,
    private router: Router) { }

  ngOnInit() {
    this.currentTicket = this.mainService.currentTicketInfo;
    if (Object.keys(this.currentTicket).length === 0) {
      this.router.navigate(['/', 'travel-request-dashboard']);
    }
  }

  ngOnDestroy() {
    this.mainService.currentTicketInfo = {};
  }

  goBack() {
    this.utilityService.back();
  }

  downloadAsPDF() {
    var element = document.getElementById('travelTicketReport');
    var opt = {
      margin:       0.25,
      filename:     'travel-ticket.pdf',
      image:        { type: 'jpeg', quality: 0.98 },
      html2canvas:  { scale: 5 },
      jsPDF:        { unit: 'in', format: 'a4', orientation: 'portrait' }
    };

    html2pdf().from(element).set(opt).save();
  }
}
