import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTravelRequestDetailsComponent } from './view-travel-request-details.component';

describe('ViewTravelRequestDetailsComponent', () => {
  let component: ViewTravelRequestDetailsComponent;
  let fixture: ComponentFixture<ViewTravelRequestDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTravelRequestDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTravelRequestDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
