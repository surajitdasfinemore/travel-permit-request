import { HeaderService } from './../services/header.service';
import { ConstantService } from 'src/app/services/constant.service';
import { MainService } from './../main.service';
import { Router } from '@angular/router';
import { UtilityService } from './../services/utility.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-travel-request-dashboard',
  templateUrl: './travel-request-dashboard.component.html',
  styleUrls: ['./travel-request-dashboard.component.css']
})
export class TravelRequestDashboardComponent implements OnInit, OnDestroy {

  subscriptions = [];
  searchKey = '';
  requestData = [
    {
      field1: 'Field 1',
      field2: 'Field 2',
      field3: 'Field 3',
      field4: 'Field 4',
      field5: 'Field 5',
      field6: 'Field 6',
      field7: 'Field 7',
    },
    {
      field1: 'Field 1',
      field2: 'Field 2',
      field3: 'Field 3',
      field4: 'Field 4',
      field5: 'Field 5',
      field6: 'Field 6',
      field7: 'Field 7',
    },
    {
      field1: 'Field 1',
      field2: 'Field 2',
      field3: 'Field 3',
      field4: 'Field 4',
      field5: 'Field 5',
      field6: 'Field 6',
      field7: 'Field 7',
    },
    {
      field1: 'Field 1',
      field2: 'Field 2',
      field3: 'Field 3',
      field4: 'Field 4',
      field5: 'Field 5',
      field6: 'Field 6',
      field7: 'Field 7',
    },
    {
      field1: 'Field 1',
      field2: 'Field 2',
      field3: 'Field 3',
      field4: 'Field 4',
      field5: 'Field 5',
      field6: 'Field 6',
      field7: 'Field 7',
    },
    {
      field1: 'Field 1',
      field2: 'Field 2',
      field3: 'Field 3',
      field4: 'Field 4',
      field5: 'Field 5',
      field6: 'Field 6',
      field7: 'Field 7',
    },
    {
      field1: 'Field 1',
      field2: 'Field 2',
      field3: 'Field 3',
      field4: 'Field 4',
      field5: 'Field 5',
      field6: 'Field 6',
      field7: 'Field 7',
    },
    {
      field1: 'Field 1',
      field2: 'Field 2',
      field3: 'Field 3',
      field4: 'Field 4',
      field5: 'Field 5',
      field6: 'Field 6',
      field7: 'Field 7',
    },
    {
      field1: 'Field 1',
      field2: 'Field 2',
      field3: 'Field 3',
      field4: 'Field 4',
      field5: 'Field 5',
      field6: 'Field 6',
      field7: 'Field 7',
    },
    {
      field1: 'Field 1',
      field2: 'Field 2',
      field3: 'Field 3',
      field4: 'Field 4',
      field5: 'Field 5',
      field6: 'Field 6',
      field7: 'Field 7',
    },
    {
      field1: 'Field 1',
      field2: 'Field 2',
      field3: 'Field 3',
      field4: 'Field 4',
      field5: 'Field 5',
      field6: 'Field 6',
      field7: 'Field 7',
    }
  ];
  requestDataCopy = [];

  actionName = 'view';
  constructor(private utilityService: UtilityService,
    private router: Router,
    private constantService: ConstantService,
    private mainService: MainService,
    private headerService: HeaderService) { }

  ngOnInit() {
    this.getAllDataByUser();
    this.requestDataCopy = JSON.parse(JSON.stringify(this.requestData));
    if (this.headerService.userType === this.constantService.adminRoleName) {
      this.actionName = 'review';
    }
  }

  getAllDataByUser() {
    const sub = this.mainService.getTravelRequestList(this.constantService.userName).subscribe(res => {
      this.requestData = res;
    }, err => {
      console.error(err);
    });
    this.subscriptions.push(sub);
  }

  goBack() {
    this.utilityService.back();
  }

  newTravelRequest() {
    this.router.navigate(['/', 'create-new-travel-request']);
  }

  viewTicketDetails(currentTicket) {
    this.mainService.currentTicketInfo = currentTicket;
    if (this.headerService.userType === this.constantService.adminRoleName) {
      this.router.navigate(['/', 'approve']);
    } else {
      this.router.navigate(['/', 'view-travel-request-details']);
    }
  }

  searchRequestByKB(event) {
    if (event.keyCode === 13) {
      this.searchReqeust();
    }
  }

  searchReqeust() {
    const sub = this.mainService.getSearchTravelRequest(this.searchKey).subscribe(res => {
      this.requestData = res;
    }, err => {
      console.error(err);
    });
    this.subscriptions.push(sub);
  }

  clearSearch() {
    this.searchKey = '';
    this.requestData = JSON.parse(JSON.stringify(this.requestDataCopy));
  }

  searchKeyChange() {
    if (this.searchKey.trim() === '') {
      this.requestData = JSON.parse(JSON.stringify(this.requestDataCopy));
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
