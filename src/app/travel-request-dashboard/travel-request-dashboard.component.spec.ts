import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TravelRequestDashboardComponent } from './travel-request-dashboard.component';

describe('TravelRequestDashboardComponent', () => {
  let component: TravelRequestDashboardComponent;
  let fixture: ComponentFixture<TravelRequestDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TravelRequestDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TravelRequestDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
