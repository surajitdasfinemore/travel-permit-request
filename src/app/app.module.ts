import { AdminGuard } from './guards/admin.guard';
import { MainService } from './main.service';
import { CommentBoxComponent } from './components/comment-box/comment-box.component';
import { DragDropDirective } from './directives/drag-drop.directive';
import { UtilityService } from './services/utility.service';
import { ConstantService } from './services/constant.service';
import { LoadingService } from './services/loading.service';
import { HeaderService } from './services/header.service';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { InformationBoxComponent } from './components/information-box/information-box.component';
import { MessageBoxComponent } from './components/message-box/message-box.component';
import { ConfirmationBoxComponent } from './components/confirmation-box/confirmation-box.component';
import { LoadingComponent } from './components/loading/loading.component';
import { ViewDetailsComponent } from './components/view-details/view-details.component';
import { UnauthorisedComponent } from './components/unauthorised/unauthorised.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxEchartsModule } from 'ngx-echarts';
import { AgmCoreModule } from '@agm/core';
import { ChartsModule } from 'ng2-charts';
import { MatDialogModule } from '@angular/material';
import { TravelRequestComponent } from './travel-request/travel-request.component';
import { TravelRequestDashboardComponent } from './travel-request-dashboard/travel-request-dashboard.component';
import { ViewTravelRequestDetailsComponent } from './view-travel-request-details/view-travel-request-details.component';
import { PreviewDocsComponent } from './preview-docs/preview-docs.component';
import { ApproveRejectComponent } from './approve-reject/approve-reject.component';

// routes are here
const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'travel-request-dashboard',
    component: TravelRequestDashboardComponent
  },
  {
    path: 'create-new-travel-request',
    component: TravelRequestComponent
  },
  {
    path: 'view-travel-request-details',
    component: ViewTravelRequestDetailsComponent
  },
  {
    path: 'approve',
    component: ApproveRejectComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'unauthorised',
    component: UnauthorisedComponent
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    InformationBoxComponent,
    MessageBoxComponent,
    ConfirmationBoxComponent,
    LoadingComponent,
    ViewDetailsComponent,
    UnauthorisedComponent,
    PageNotFoundComponent,
    DragDropDirective,
    CommentBoxComponent,
    TravelRequestComponent,
    TravelRequestDashboardComponent,
    ViewTravelRequestDetailsComponent,
    PreviewDocsComponent,
    ApproveRejectComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBDvD8ZcDvrrQvdkXRe_zQ-jgzAS3IT8J8'
    }),
    RouterModule.forRoot(routes, {useHash: true}),
    MatDialogModule,
    ChartsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ],
  providers: [
    HeaderService,
    ConstantService,
    UtilityService,
    LoadingService,
    MainService
  ],
  entryComponents: [
    ConfirmationBoxComponent,
    InformationBoxComponent,
    MessageBoxComponent,
    CommentBoxComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
